# esp32_test

Example of ESP-IDF (FreeRTOS) program for ESP-WROOM-32 based board

* Blink 3 LEDs one at a time, running in different tasks
* Move a 28BYJ-48 stepper using ULN2003 driver
* Connect to WIFI network
* Send commands via UART0
    * motor on|off
    * blink on|off
    * wificonn SSID PASSWORD
    * wifidisconn
* Send commands via HTTP GET
    * http://[IP ADDRESS]/api?motor=on|off
    * http://[IP ADDRESS]/api?blink=on|off