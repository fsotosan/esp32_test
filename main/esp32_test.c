/* 
    Simple task test
    Two tasks running on the same core
    Each task blinks a different LED with period 1ms
*/
#include <stdio.h>
#include <string.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "driver/gpio.h"
#include "driver/uart.h"
#include "freertos/semphr.h"
#include "esp_wifi.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "nvs_flash.h"
#include "esp_http_server.h"

// GPIO LED pins

#define LED_0    26
#define LED_1    27
// Built in blue LED in devkit
#define LED_2    2  

#define GPIO_OUTPUT_PIN_SEL  ((1ULL<<LED_0) | (1ULL<<LED_1) | (1ULL<<LED_2) | (1ULL<<STEPPER_IN_1)| (1ULL<<STEPPER_IN_2)| (1ULL<<STEPPER_IN_3)| (1ULL<<STEPPER_IN_4))
//#define GPIO_INPUT_PIN_SEL  (1ULL<<BUTTON)
#define LOW    0
#define HIGH   1

// GPIO Pins to the ULN2003 driver

#define STEPPER_IN_1    14
#define STEPPER_IN_2    32
#define STEPPER_IN_3    33
#define STEPPER_IN_4    12

// UART

#define UART_NUM UART_NUM_0
#define UART_BUF_SIZE (1024)
#define UART_RD_BUF_SIZE (UART_BUF_SIZE)

// WIFI

#define WIFI_MAX_SSID_LENGTH 32
#define WIFI_MAX_PWD_LENGTH 16
#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1
#define WIFI_MAXIMUM_RETRY  10

int wifi_pending_connect = 0;
int wifi_pending_disconnect = 0;
char wifi_ssid[32];
char wifi_password[64];
static int wifi_s_retry_num = 0;
static EventGroupHandle_t wifi_s_wifi_event_group;
static const char *WIFI_TAG = "wifi station";

// HTTP

static httpd_handle_t http_server = NULL;
static const char *HTTP_TAG = "httpd";

// COMMMANDS

#define MAX_COMMAND_LENGTH (64)
#define MAX_COMMAND_LENGTH (64)
static QueueHandle_t uart0_queue;   // UART queue
char command[MAX_COMMAND_LENGTH];
int command_length = 0;
int command_complete = 0;

// Configuration state
int blink = 1;
int motor_on = 1;


int debugNum = 0;   // Just for debugging. This value will be returned when command debug is called

SemaphoreHandle_t xBinarySemaphoreLEDs;     // Only one LED ON 
SemaphoreHandle_t xBinarySemaphoreCommand;  // For handling the commands reception

// Blinks the input LED with the given period if blink == 1
// In order to have only one LED on at the same time a semaphore is used
void vBlinkLedExclusive(int xGPIO_PIN, int xPERIOD_MS, SemaphoreHandle_t xSem)
{
    TickType_t xDelay = pdMS_TO_TICKS( xPERIOD_MS / 2 );
    for (;;) {
        xSemaphoreTake(xSem,portMAX_DELAY);
        if (blink == 1)
        {     
            gpio_set_level( xGPIO_PIN, HIGH );
            vTaskDelay( xDelay );
            gpio_set_level( xGPIO_PIN, LOW );
    
        } 
        xSemaphoreGive(xSem);
        vTaskDelay( xDelay );
    }
}

// Command interpreter
// Update configuration basing on input commands. 
// Return OK or ERROR
static void vProcessCommandTask(void *pvParameters)
{
    const TickType_t xDelay = pdMS_TO_TICKS( 250 );
    char* cmd = (char*) malloc(MAX_COMMAND_LENGTH);
    int i,l,k;
    int err;
    int ssid_ok = 0;
    char* dbg = (char*) malloc(128);
    for( ;; )
    {
        if (command_complete == 1) {

            xSemaphoreTake(xBinarySemaphoreCommand,portMAX_DELAY);
            for (i=0;i<MAX_COMMAND_LENGTH;i++) 
            {
                cmd[i] = command[i];
            }
            //strcpy(cmd,command);
            bzero(command,MAX_COMMAND_LENGTH);
            command_length = 0;
            command_complete = 0;

            xSemaphoreGive(xBinarySemaphoreCommand);

            // Process ccommand stored in cmd
            err = 0;
            if (strcmp(cmd,"blink off") == 0) {
                blink = 0;               
            } else if (strcmp(cmd,"blink on") == 0) {
                blink = 1;
            } else if (strcmp(cmd,"motor on") == 0) {
                motor_on = 1;
            } else if (strcmp(cmd,"motor off") == 0) {
                motor_on = 0;
            } else if (strcmp(cmd,"debug") == 0) {
                i = sprintf(dbg, "debug=%d\r", debugNum);
                uart_write_bytes( UART_NUM, dbg, i );    
            } else if (strncmp(cmd,"wificonn",8) == 0) {
                // Extract SSID and password
                // wificonnect SSID PASSWORD
                l = strlen(cmd);
                ssid_ok = 0;
                if (l < 19) {
                    // Assume password has min 8 characters SSID has min 1 (8+1+1+1+8=19)
                    err = 1;    
                } else {
                    // Parse command to extract SSID and PWD
                    // into wifi_ssid and wifi_password variables
                    bzero( wifi_ssid, WIFI_MAX_SSID_LENGTH );
                    bzero( wifi_password, WIFI_MAX_PWD_LENGTH );
                    k = 0;
                    for (i=9;i<l;i++) {
                        if (ssid_ok == 0) {
                            if(cmd[i] == ' ') {
                                ssid_ok = 1;
                                k = 0;
                            } else {
                                wifi_ssid[k++] = cmd[i];
                            }
                        } else {
                            wifi_password[k++] = cmd[i];
                        }
                    }
                    wifi_pending_connect = 1;
                    i = sprintf(dbg, "SSID %s PWD %s\r", wifi_ssid, wifi_password);
                    uart_write_bytes( UART_NUM, dbg, i );
                }
            } else if (strncmp(cmd,"wifidisconn",11) == 0) {
                wifi_pending_disconnect = 1;
            } else {
                i = sprintf(dbg, "Unexpected %s", cmd);
                uart_write_bytes( UART_NUM, dbg, i );
                err = 1;
            }

            // Give command feedback
            if (err == 0) {
                uart_write_bytes( UART_NUM, "OK\r", 4 );    
            } else {
                uart_write_bytes( UART_NUM, "ERROR\r", 7 );    
            }

        }
                
        vTaskDelay( xDelay );
    }

}



// Process UART events (only UART_DATA, ignore others)
// by reading periodically uart0_queue (filled in by UART's ISR) 
// Expects CR or LF as command completion
// Supports Backspace to delete characters
// Updates global variable command as characters are received
// and indicates command completion through global variable command_complete = 1
static void vUartReceiveEventTask(void *pvParameters)
{
    const TickType_t xDelay = pdMS_TO_TICKS( 250 );
    uart_event_t event;
    uint8_t* dtmp = (uint8_t*) malloc(UART_RD_BUF_SIZE);
    int i;
    char c;
    for(;;) {
        //Waiting for UART event.
        if(xQueueReceive( uart0_queue, (void * )&event, (portTickType)portMAX_DELAY ) ) {
            bzero(dtmp, UART_RD_BUF_SIZE);
            switch(event.type) {
                case UART_DATA:
                    uart_read_bytes( UART_NUM, dtmp, event.size, portMAX_DELAY );
                    // Protect global variable command with semaphore 
                    // so it is not used while it is being populated
                    xSemaphoreTake(xBinarySemaphoreCommand,portMAX_DELAY);
                    if (command_complete == 1) {
                        // There is still a command pending
                        // cannot accept more inputs
                        uart_write_bytes( UART_NUM, "ERROR: TOO FAST\r\n", 17 );        
                    } else {
                        for (i=0;i<event.size;i++) {
                            if (command_length > MAX_COMMAND_LENGTH) {
                                // Command too long
                                uart_write_bytes( UART_NUM, "ERROR: COMMAND TOO LONG\r\n", 25 );
                                bzero(command,MAX_COMMAND_LENGTH);
                                command_length = 0;
                            } else {
                                c = (char)dtmp[i];
                                if ((c == 10)|(c == 13)) {
                                    // CR or LF. Command completion indicator
                                    command_complete = 1;
                                    command[command_length++] = 0;
                                } else if ((c == 8) && (command_length > 0) ) {
                                    // Backspace. Delete last character
                                    command[--command_length] = 0;
                                } else {
                                    // Add character to command
                                    command[command_length++] = (char)c;
                                }
                            }
                        }
                    }
                    // Release semaphore
                    xSemaphoreGive(xBinarySemaphoreCommand);
                    break;
                default:
                    // Ignore other UART events for the moment
                    break;
            }
        }
        vTaskDelay( xDelay );
    }
}

// Initial UART configuration
void uart_setup()
{
    uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_APB,
    };
    //Install UART driver, and get the queue.
    uart_driver_install (UART_NUM, UART_BUF_SIZE * 2, UART_BUF_SIZE * 2, 20, &uart0_queue, 0 );
    uart_param_config( UART_NUM, &uart_config );
    uart_set_pin( UART_NUM, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE );

}


static void wifi_event_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    // Taken from https://github.com/espressif/esp-idf/blob/master/examples/wifi/getting_started/station/main/station_example_main.c

    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        if (wifi_s_retry_num < WIFI_MAXIMUM_RETRY) {
            esp_wifi_connect();
            wifi_s_retry_num++;
            ESP_LOGI(WIFI_TAG, "retry to connect to the AP");
        } else {
            xEventGroupSetBits(wifi_s_wifi_event_group, WIFI_FAIL_BIT);
        }
        ESP_LOGI(WIFI_TAG,"connect to the AP fail");
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(WIFI_TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        wifi_s_retry_num = 0;
        xEventGroupSetBits(wifi_s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}

static esp_err_t http_get_handler_api(httpd_req_t *req)
{

    // Taken from https://github.com/espressif/esp-idf/blob/639e7ad494d93fc82159f9fe854041bc43a96d5f/examples/protocols/http_server/simple/main/main.c

    char*  buf;
    char*  cmd;
    size_t buf_len;
    int i,k;
    static char cmds[2][MAX_COMMAND_LENGTH] = {"motor", "blink"};

    /* Get header value string length and allocate memory for length + 1,
     * extra byte for null termination */
    /*
    buf_len = httpd_req_get_hdr_value_len(req, "Host") + 1;
    if (buf_len > 1) {
        buf = malloc(buf_len);
        // Copy null terminated value string into buffer 
        if (httpd_req_get_hdr_value_str(req, "Host", buf, buf_len) == ESP_OK) {
            ESP_LOGI(TAG, "Found header => Host: %s", buf);
        }
        free(buf);
    }

    buf_len = httpd_req_get_hdr_value_len(req, "Test-Header-2") + 1;
    if (buf_len > 1) {
        buf = malloc(buf_len);
        if (httpd_req_get_hdr_value_str(req, "Test-Header-2", buf, buf_len) == ESP_OK) {
            ESP_LOGI(TAG, "Found header => Test-Header-2: %s", buf);
        }
        free(buf);
    }

    buf_len = httpd_req_get_hdr_value_len(req, "Test-Header-1") + 1;
    if (buf_len > 1) {
        buf = malloc(buf_len);
        if (httpd_req_get_hdr_value_str(req, "Test-Header-1", buf, buf_len) == ESP_OK) {
            ESP_LOGI(TAG, "Found header => Test-Header-1: %s", buf);
        }
        free(buf);
    }
    */

    /* Read URL query string length and allocate memory for length + 1,
     * extra byte for null termination */
    buf_len = httpd_req_get_url_query_len(req) + 1;
    if (buf_len > 1) {
        buf = malloc(buf_len);
        if (httpd_req_get_url_query_str(req, buf, buf_len) == ESP_OK) {
            ESP_LOGI(HTTP_TAG, "Found URL query => %s", buf);
            char param[32];
            /* Get value of expected key from query string */
            for (i=0;i<2;i++) {
                if (httpd_query_key_value(buf, cmds[i], param, sizeof(param)) == ESP_OK) {
                    cmd = malloc(sizeof(cmds[i]) + sizeof(param));
                    xSemaphoreTake(xBinarySemaphoreCommand,portMAX_DELAY);
                    command_length = sprintf(cmd, "%s %s", cmds[i], param);
                    ESP_LOGI(HTTP_TAG, "Command: %s", cmd);
                    for(k=0;k<command_length;k++)
                    {
                        command[k] = cmd[k];
                    }
                    command_complete = 1;
                    xSemaphoreGive(xBinarySemaphoreCommand);
                    httpd_resp_send(req, cmd, HTTPD_RESP_USE_STRLEN);
                    return ESP_OK;
                }    
            }
        }
        free(buf);
    }

    httpd_resp_send(req, "ERROR", HTTPD_RESP_USE_STRLEN);
    return ESP_OK;

    /* Set some custom headers */
    /*
    httpd_resp_set_hdr(req, "Custom-Header-1", "Custom-Value-1");
    httpd_resp_set_hdr(req, "Custom-Header-2", "Custom-Value-2");
    */
    /* Send response with custom headers and body set as the
     * string passed in user context*/
    //const char* resp_str = (const char*) req->user_ctx;
    

    /* After sending the HTTP response the old HTTP request
     * headers are lost. Check if HTTP request headers can be read now. */
    /*
    if (httpd_req_get_hdr_value_len(req, "Host") == 0) {
        ESP_LOGI(HTTP_TAG, "Request headers lost");
    }
    */
    
}

static const httpd_uri_t http_uri_api = {
    .uri       = "/api",
    .method    = HTTP_GET,
    .handler   = http_get_handler_api,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = "Hello World!"
};


static httpd_handle_t http_start_webserver(void)
{
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.lru_purge_enable = true;

    // Start the httpd server
    ESP_LOGI(HTTP_TAG, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Set URI handlers
        ESP_LOGI(HTTP_TAG, "Registering URI handlers");
        httpd_register_uri_handler(server, &http_uri_api);
        //httpd_register_uri_handler(server, &echo);
        //httpd_register_uri_handler(server, &ctrl);
        /*
        #if CONFIG_EXAMPLE_BASIC_AUTH
        httpd_register_basic_auth(server);
        #endif
        */
        return server;
    }

    ESP_LOGI(HTTP_TAG, "Error starting server!");
    return NULL;
}

static void http_stop_webserver(httpd_handle_t server)
{
    // Stop the httpd server
    httpd_stop(server);
}

static void wifi_disconnect_handler(void* arg, esp_event_base_t event_base,
                                    int32_t event_id, void* event_data)
{
    httpd_handle_t* server = (httpd_handle_t*) arg;
    if (*server) {
        ESP_LOGI(HTTP_TAG, "Stopping webserver");
        http_stop_webserver(*server);
        *server = NULL;
    }
}

static void wifi_connect_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    httpd_handle_t* server = (httpd_handle_t*) arg;
    if (*server == NULL) {
        ESP_LOGI(HTTP_TAG, "Starting webserver");
        *server = http_start_webserver();
    }
}


void wifi_init_sta(void)
{
    // Taken from https://github.com/espressif/esp-idf/blob/master/examples/wifi/getting_started/station/main/station_example_main.c

    wifi_s_wifi_event_group = xEventGroupCreate();

    ESP_ERROR_CHECK(esp_netif_init());

    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    esp_event_handler_instance_t instance_any_id;
    esp_event_handler_instance_t instance_got_ip;
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &wifi_event_handler,
                                                        NULL,
                                                        &instance_any_id));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_STA_GOT_IP,
                                                        &wifi_event_handler,
                                                        NULL,
                                                        &instance_got_ip));

    wifi_config_t wifi_config = {
        .sta = {
            //.ssid = wifi_ssid,
            //.password = wifi_password,
            /* Setting a password implies station will connect to all security modes including WEP/WPA.
             * However these modes are deprecated and not advisable to be used. Incase your Access point
             * doesn't support WPA2, these mode can be enabled by commenting below line */
	     //.threshold.authmode = WIFI_AUTH_WPA2_PSK,

            .pmf_cfg = {
                .capable = true,
                .required = false
            },
        },
    };

    strcpy((char *)wifi_config.sta.ssid, wifi_ssid);
    strcpy((char *)wifi_config.sta.password, wifi_password);

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

    ESP_LOGI(WIFI_TAG, "wifi_init_sta finished.");

    /* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed for the maximum
     * number of re-tries (WIFI_FAIL_BIT). The bits are set by event_handler() (see above) */
    EventBits_t bits = xEventGroupWaitBits(wifi_s_wifi_event_group,
            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
     * happened. */
    if (bits & WIFI_CONNECTED_BIT) {
        ESP_LOGI(WIFI_TAG, "connected to ap SSID:%s password:%s",
                 wifi_ssid, wifi_password);
    } else if (bits & WIFI_FAIL_BIT) {
        ESP_LOGI(WIFI_TAG, "Failed to connect to SSID:%s, password:%s",
                 wifi_ssid, wifi_password);
    } else {
        ESP_LOGE(WIFI_TAG, "UNEXPECTED EVENT");
    }
    
    /* The event will not be processed after unregister */
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &wifi_connect_handler, &http_server));
    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED, &wifi_disconnect_handler, &http_server));
    http_server = http_start_webserver();
    vEventGroupDelete(wifi_s_wifi_event_group);
}




/* Blink LED_0 with period 250ms */
void vTaskLed0(void* pvParameters)
{
    vBlinkLedExclusive(LED_0,250,xBinarySemaphoreLEDs);
}

/* Blink LED_1 with period 250ms */
void vTaskLed1(void* pvParameters)
{
    vBlinkLedExclusive(LED_1,250,xBinarySemaphoreLEDs);
}

/* Blink LED_2 with period 250ms */
void vTaskLed2(void* pvParameters)
{
    vBlinkLedExclusive(LED_2,250,xBinarySemaphoreLEDs);
}

/* Move stepper clockwise only if global variable motor_on == 1 */
void vTaskStepper(void* pvParameters)
{
    // 500ms
    const TickType_t xDelay = pdMS_TO_TICKS( 10 ); // 100Hz
    const int steps_cw[] = { 0b1001, 0b1100, 0b0110, 0b0011 };      // Double step CW
    //const int steps_cw[] = { 0b1000, 0b0100, 0b0010, 0b0001 };    // Single step CW
    //const int steps_ccw[] = { 0b0011, 0b0110, 0b1100, 0b1001 };   // Double step CCW

    debugNum = xDelay;

    int i = 0;

    for( ;; )
    {
        if (motor_on == 1) 
        {
            int step = steps_cw[i++];
            if (i == 4) i = 0;
            gpio_set_level( STEPPER_IN_1, (step & ( 1 << 0 )) >> 0 );
            gpio_set_level( STEPPER_IN_2, (step & ( 1 << 1 )) >> 1 );
            gpio_set_level( STEPPER_IN_3, (step & ( 1 << 2 )) >> 2 );
            gpio_set_level( STEPPER_IN_4, (step & ( 1 << 3 )) >> 3 );
        }
        vTaskDelay( xDelay );
    }

}

void vTaskWifiConn(void* pvParameters)
{
    // 1000ms
    const TickType_t xDelay = pdMS_TO_TICKS( 1000 ); // 100Hz
    
    for( ;; )
    {
        if (wifi_pending_connect == 1) {
            wifi_pending_connect = 0;
            wifi_init_sta();
        } else if (wifi_pending_disconnect == 1) {
            wifi_pending_disconnect = 0;
            ESP_ERROR_CHECK( esp_wifi_stop() );
        }
        vTaskDelay( xDelay );
    }

}

void wifi_setup()
{
    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
}


/* Initial GPIO configuration */
void gpio_setup() 
{
    // OUT
    gpio_config_t io_conf;
    //disable interrupt
    io_conf.intr_type = GPIO_INTR_DISABLE;
    //set as output mode
    io_conf.mode = GPIO_MODE_OUTPUT;
    //bit mask of the pins 
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
    //disable pull-down mode
    io_conf.pull_down_en = 0;
    //disable pull-up mode
    io_conf.pull_up_en = 0;
    //configure GPIO with the given settings
    gpio_config( &io_conf );

    // IN
    /*
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
    gpio_config( &io_conf );
    */

}

/* main */
void app_main(void)
{
    TaskHandle_t th1, th2, th3, th4, th5, th6, th7;

    gpio_setup();    
    uart_setup();
    wifi_setup();
    
    xBinarySemaphoreLEDs = xSemaphoreCreateBinary();
    xBinarySemaphoreCommand = xSemaphoreCreateBinary();

    // Tasks specifying which core to be run on
    
    xTaskCreatePinnedToCore( vTaskLed0, "vTaskLed0", configMINIMAL_STACK_SIZE, NULL, 1, &th1, 1 );
    xTaskCreatePinnedToCore( vTaskLed1, "vTaskLed1", configMINIMAL_STACK_SIZE, NULL, 1, &th2, 1 );
    xTaskCreatePinnedToCore( vTaskLed2, "vTaskLed2", configMINIMAL_STACK_SIZE, NULL, 1, &th3, 1 );
    
    // Tasks not specifying which core to be run on
    xTaskCreate( vTaskStepper, "vTaskStepper", 10*configMINIMAL_STACK_SIZE, NULL, 3, &th4 );
    xTaskCreate( vUartReceiveEventTask, "vUartReceiveEventTask", 2048, NULL, 2, &th5 );
    xTaskCreate( vProcessCommandTask, "vProcessCommandTask", 2048, NULL, 2, &th6 );

    xTaskCreate( vTaskWifiConn, "vTaskWifiConn", 4096, NULL, 1, &th7 );

    xSemaphoreGive(xBinarySemaphoreLEDs);
    xSemaphoreGive(xBinarySemaphoreCommand);

}
